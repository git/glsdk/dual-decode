/*
 *  Copyright (c) 2012-2013, Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Contact information for paper mail:
 *  Texas Instruments
 *  Post Office Box 655303
 *  Dallas, Texas 75265
 *  Contact information:
 *  http://www-k.ext.ti.com/sc/technical-support/product-information-centers.htm
 *  ?DCMP=TIHomeTracking&HQS=Other+OT+home_d_contact
 *  ============================================================================
 *
 */

/**
 * @file 		main.c
 *
 * @brief 		Main Application calling the GUI.
 */

#include <gtk/gtk.h>
#include <gst/gst.h>
#include <string.h>
#include <linux/limits.h>

#include "common.h"
#include "gui.h"

gint decodeMode 		= DECODE_MODE_NONE; 
gchar *filename[]		= {NULL, NULL};
gboolean version		= FALSE;
gchar *sink				= "xvimagesink";
gchar **files			= NULL;

const gchar *version_text = "0.2"; 

gint setAbsolutePathname (gchar *file, gchar **uri)									 
{		
	gchar *realPathname = NULL;
	gint ret 		= ERR_SUCCESS;

	if (NULL == uri){
		return ERR_INVALIDPARAM;
	}	

	if (NULL == file){
		g_printerr ("BUG: file: " __FILE__ " line: %d" "\n", __LINE__);		
		return ERR_BUG;
	}

	*uri = (gchar *) g_malloc (sizeof(gchar) * PATH_MAX_LEN);	   
	realPathname = realpath (file, realPathname);
	if (NULL == realPathname) {
		g_printerr ("File %s not found\n", file);
		ret = ERR_FILENOTFOUND;
		goto destroy;	
	}
	g_snprintf (*uri,PATH_MAX_LEN,"file://%s",realPathname);

	goto last;	

destroy:
	g_free (*uri);
	*uri = NULL;
last:
	free (realPathname);
	return ret;																   
}														  

gint processOpts()
{
	gint fileCount 		= 0;
	gchar *uri			= NULL;
	gint ret			= ERR_SUCCESS;
	if (version) {
		g_print ("VERSION %s\n", version_text);
		exit (ERR_SUCCESS);
	}

	if (NULL == files){
		return ERR_SUCCESS;
	}
	while (NULL != files[fileCount]){
		if (fileCount < NUM_MAX_FILES){
			uri = NULL;
			ret = setAbsolutePathname (files[fileCount], &uri);
			if (ERR_SUCCESS != ret){
				return ret;
			}
			decodeMode++;
			filename[fileCount] = uri;
		}else{
			g_printerr ("Ignoring file : %s\n",files[fileCount]);
		}
		fileCount++;
	}
    return ret;
}

gint processArgs (int *argc, char **argv[])
{
	GOptionEntry entries[] =														
	{																			   
		{ "version", 'v', 0, G_OPTION_ARG_NONE, &version,							 
		"Version of the application", NULL },								   
		{ "sink", 's', 0, G_OPTION_ARG_STRING, &sink,								 
		"Use S for video sink", "S" },										  
		{ G_OPTION_REMAINING,0, 0, G_OPTION_ARG_FILENAME_ARRAY, &files, 
			"Files", "filenames"},
		{ NULL }																	  
	};	 
	GError *error = NULL;													   
	GOptionContext *context;   
 
	/*Create a GOption context*/												
	context = g_option_context_new ("- Dual Decode application");			   
	g_option_context_add_main_entries (context, entries, NULL);				 
	g_option_context_add_group (context, gst_init_get_option_group ());  	
																				
	/* Parse the command line arguments and options*/						   
	if (!g_option_context_parse (context, argc, argv, &error)) {			   
		g_printerr ("option parsing failed: %s\n", error->message);				  
		g_error_free (error);
	  	return ERR_OPTIONPARSE;																 
	}															   
	return processOpts ();
}

	  

int main (int argc, char *argv[])
{
	gint ret = ERR_SUCCESS;

	ret = processArgs (&argc, &argv);
	if(ERR_SUCCESS != ret){
		return ret;
	}

	if (FALSE == DualDecode_initGUI(&argc,&argv)) {
		/*replace with a debug*/
		g_printerr("X is not found. Exiting\n");
		return ERR_XNOTFOUND;
	}
	DualDecode_startApplication();
}
