/*
 *  Copyright (c) 2012-2013, Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Contact information for paper mail:
 *  Texas Instruments
 *  Post Office Box 655303
 *  Dallas, Texas 75265
 *  Contact information:
 *  http://www-k.ext.ti.com/sc/technical-support/product-information-centers.htm
 *  ?DCMP=TIHomeTracking&HQS=Other+OT+home_d_contact
 *  ============================================================================
 *
 */

/**
 * @file 		gui.h
 *
 * @brief 		Declares the data structures and the functions exported by the 
 *				GUI. 
 */

#ifndef __GUI_H__
#define __GUI_H__

#include <gtk/gtk.h>
#include <gst/gst.h>
#include "config.h"
/******************************************************************************
 
                         Structure Definitions
 
*****************************************************************************/

/**
 *	@brief			This structure defines all the components and the signals
 *					for a Dual-Decode GUI window. There will be two such
 *					windows when the application is running 
 */
typedef struct{

	/*The Window and its delete-event signal handler*/
	GtkWidget *window;
	gulong windowCloseSignal;
 
    /*Image sink for the window*/
    GstElement *sink;

	/*The Seekbar widget and the value-changed signal handler			*/
	/*value-changed handler is called when media is seeked using seekbar*/
	GtkWidget *seekScale;
	gulong seekSignal;

	/*File Open button and clicked signal handler*/
	GtkWidget *openButton;
	gulong openSignal;

	/*Media control buttons and clicked signal handlers*/
	GtkWidget *playButton;
	gulong playSignal;
	GtkWidget *pauseButton;
	gulong pauseSignal;
	GtkWidget *stopButton;
	gulong stopSignal;
	GtkWidget *forwardButton;
	gulong forwardSignal;
	GtkWidget *rewindButton;
	gulong rewindSignal;

	/*Help Button and clicked signal handler*/
	GtkWidget *helpButton;
	gulong helpSignal;

	/*The decode mode switch button and the clicked signal handler*/
	GtkWidget *switchButton;
	gulong switchSignal;

	/*Labels to display media playback status and position in media*/
	GtkWidget *statusLabel;
	GtkWidget *timeLabel;

	/*The timer signal to report position in media every*/
	/*REPORT_INTERVAL seconds							*/
	gulong timerSignal;

	/*The draw area where the video is displayed*/
	GtkWidget *drawArea;

	/*The state of the controls*/
	gint activeFlag;
	gint modeFlag;
} GUIWindow;



/******************************************************************************

                                Macros

 *****************************************************************************/
/*To index the static GUIWindow and Pipeline structures			*/
/*There are two of them, since it is a dual-decode application	*/
#define DECODER_INDEX_SINGLE 0
#define DECODER_INDEX_DOUBLE 1

/*ratio of screen size per window*/
#define SCREEN_WIDTH_FACTOR 3
#define SCREEN_HEIGHT_FACTOR 2

/*Whether the control buttons are active*/
#define CONTROL_MODE_ACTIVE TRUE
#define CONTROL_MODE_INACTIVE FALSE

#define CONTROL_MODE_STOPPED 0
#define CONTROL_MODE_PLAYING 1
#define CONTROL_MODE_PAUSED 2
#define CONTROL_MODE_NO_FILE 4

#define LABEL_TEXT_NO_FILE "No File"
#define LABEL_TEXT_PLAYING "Playing"
#define LABEL_TEXT_STOPPED "Stopped"
#define LABEL_TEXT_PAUSED "Paused"

#define TIME_LABEL_ORIGIN "--:--:--/--:--:--"

#define FORWARD 3000000000
#define REWIND 3000000000
#define SWITCH_TEXT_SINGLE "Single Decode"
#define SWITCH_TEXT_DUAL "Dual Decode"

#define TIMER_INTERVAL 1000
#define TIMER_SIGNAL_NONE 0


/******************************************************************************
 
              Statically used variables per GUI component file

 *****************************************************************************/

static GError *error 			= NULL;
static GtkBuilder *builder 		= NULL;

/******************************************************************************
 
                        Static Inline functions
 
*****************************************************************************/

/**
 * Report and then free the error, if error is not NULL
 *This function is called when a NULL is returned from
 *an allocation function
 */                              
static inline void DualDecode_checkError(){
	if(NULL == error){
		g_printerr("GError Report : Unknown error\n");
	}else{
		g_printerr("GError Report : %s\n",error->message);
		g_error_free(error);
		error = NULL;
	}
}

/*create and close a static GtkBuilder*/
static inline void DualDecode_builderClose() {
	g_object_unref(G_OBJECT(builder));
	builder = NULL;
}
static inline void DualDecode_builderCreate() {
	builder = gtk_builder_new();
	if(0 == gtk_builder_add_from_file (builder,GUI_XML,&error)){
		DualDecode_checkError();
		DualDecode_builderClose();
	}
}

/*get a widget from the builder*/
static inline GtkWidget *DualDecode_getWidget(const char *widget) {
	GtkWidget *retWidget = NULL;
	if(NULL == builder){
		retWidget = NULL;
	}else{
		retWidget = (GtkWidget *)gtk_builder_get_object(builder,widget);
	}
	return retWidget;
}

/**
 * Get a widget exclusively from the builder                      
 * this will let the application create two instances             
 * of a widget defined in the glade file.                         
 *                                                                
 * For example : this macro can be used like -                    
 *                                                                
 * widget0 = DualDecode_getWidgetExclusive("applicationWindow")   
 * widget1 = DualDecode_getWidgetExclusive("applicationWindow")   
 *                                                                
 * this will create two different widgets with the same           
 * look and feel                                                  
 */

static inline GtkWidget *DualDecode_getWidgetExclusive(const char *widget) {
	GtkWidget *retWidget = NULL;
	DualDecode_builderCreate();
	retWidget=DualDecode_getWidget(widget);
	DualDecode_builderClose();
	return retWidget;
}

/******************************************************************************
 
                        Public functions declarations
 
******************************************************************************/

/**
 *	@brief			initialises GTK if X is present
 *
 *	@param[in,out] argc		number of command line arguments
 *	@param[in,out] argv		command line arguments
 *
 *	@return			TRUE if X present, FALSE otherwise		
 *
 *	Called From		main()
 */
gboolean DualDecode_initGUI(gint *argc, char **argv[]);

/**
 *	@brief			start the application
 *
 *	Called From		main()
 */
void DualDecode_startApplication();

/**
 *	@brief			stop the application and return to main()
 *
 *	Called From		delete-event handlers of the windows
 */
void DualDecode_exitApplication();


#endif /*__GUI_H__*/
