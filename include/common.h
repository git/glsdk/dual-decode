/*
 *  Copyright (c) 2012-2013, Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Contact information for paper mail:
 *  Texas Instruments
 *  Post Office Box 655303
 *  Dallas, Texas 75265
 *  Contact information:
 *  http://www-k.ext.ti.com/sc/technical-support/product-information-centers.htm
 *  ?DCMP=TIHomeTracking&HQS=Other+OT+home_d_contact
 *  ============================================================================
 *
 */

/**
 * @file 		common.h
 *
 * @brief 		Declares global variables and utility functions
 */
#ifndef __COMMON_H__
#define __COMMON_H__

#include <linux/limits.h>
#include <glib.h>

/**
 *Extern variables which flags passed as options.
 */

/**
 * @brief     The decode mode of the appplication : single, dual or none.
 */
extern gint decodeMode; 

/**
 * @brief     Array to store filenames of the media files
 */

extern gchar *filename[];

/**
 * @brief   Stores if version of the application is asked on the command line. 
 */

extern gboolean version;

/**
 * @brief    Video sink to be used.
 */

extern gchar *sink;

/**
 * @brief     Variable for processing the filenames passed as command line 
 *            arguments.
 */

extern gchar **files;
/**
 * @brief     Version of the application.
 */

extern const gchar *version_text;


/**
 *Error codes
 */
#define ERR_SUCCESS 0
#define ERR_XNOTFOUND -1
#define ERR_INVALIDPARAM -2
#define ERR_FILENOTFOUND -3
#define ERR_OPTIONPARSE -4
#define ERR_BUG -5

/**
 *Dual decode, maximum pipelines = 2
 */
#define NUM_MAX_FILES 2
#define PATH_MAX_LEN (PATH_MAX + strlen("file://"))


/**
 * Decode mode of the application.
 * DECODE_MODE_NONE only applicable if 
 * application is started without any 
 * filename argument. This flag is used only
 * for setting the mode of the application 
 * during startup
 */
#define DECODE_MODE_NONE 0
#define DECODE_MODE_SINGLE 1 
#define DECODE_MODE_DUAL 2

/**
 * Initial position of the media file
 */
#define SEEK_START 0

#endif /*__COMMON_H__*/
