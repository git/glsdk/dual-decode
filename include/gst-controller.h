/*                                                                              
 *  Copyright (c) 2012-2013, Texas Instruments Incorporated                     
 *                                                                              
 *  Redistribution and use in source and binary forms, with or without          
 *  modification, are permitted provided that the following conditions          
 *  are met:                                                                    
 *                                                                              
 *  *  Redistributions of source code must retain the above copyright           
 *   notice, this list of conditions and the following disclaimer.              
 *                                                                              
 *  *  Redistributions in binary form must reproduce the above copyright        
 *   notice, this list of conditions and the following disclaimer in the        
 *   documentation and/or other materials provided with the distribution.       
 *                                                                              
 *  *  Neither the name of Texas Instruments Incorporated nor the names of      
 *   its contributors may be used to endorse or promote products derived        
 *   from this software without specific prior written permission.              
 *                                                                              
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,       
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR            
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,       
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,         
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,    
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR     
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,              
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                          
 *                                                                              
 *  Contact information for paper mail:                                         
 *  Texas Instruments                                                           
 *  Post Office Box 655303                                                      
 *  Dallas, Texas 75265                                                         
 *  Contact information:                                                        
 *  http://www-k.ext.ti.com/sc/technical-support/product-information-centers.htm?
 *  DCMP=TIHomeTracking&HQS=Other+OT+home_d_contact                             
 *  ============================================================================
 *                                                                              
 */                                                                             
                                                                                
/**                                                                             
 * @file  gst-controller.h                                                                
 *                                                                              
 * @brief Defines the structures and function prototypes.                                                                       
 */

#ifndef __GST_CONTROLLER_H__                                                               
#define __GST_CONTROLLER_H__
 
#include <gtk/gtk.h>
#include <gst/gst.h>


/**
 *    @brief    Defines the callback function pointer types
 *    	
 */
typedef void (*LogFunc) (gpointer data, gchar * string, ...) G_GNUC_PRINTF(2,3);
typedef void (*EOSFunc) (gpointer data);
typedef void (*ErrorFunc) (gpointer data);


/******************************************************************************

                            Structure Definitions
	
******************************************************************************/

typedef struct Pipeline {
    /* GStreamer pipeline */
    GstElement *pipe;

    /* callback functions */
    LogFunc  logFunc;
	EOSFunc eosFunc;
	ErrorFunc errFunc;

    /* Signal for a pipeline */
    gulong busSignal;
} Pipeline;

/****************************************************************************** 
                                                                                
                            Function Prototypes                              
                                                                                
******************************************************************************/ 

/**
 *    @brief    Sets the pipeline to PLAYING state.
 *    @param pipePtr     Pointer to Pipeline structure  
 *    @param filename    Uri of the file to be played
 *    @param Position    Position from where the playing should start
 *    @param drawArea1   Display in window1
 *    @param drawArea2   Display in window2 
 *    @return   TRUE if success, FALSE otherwise.
*/
gboolean DualDecode_playMedia (Pipeline *pipePtr, gchar *filename, gint64 position);

/**
 *    @brief    Sets the pipeline to PAUSED state
 *    @param pipePtr    Pointer to Pipeline structure
 *    @return    TRUE if success, FALSE otherwise
*/
gboolean DualDecode_pauseMedia (Pipeline *pipePtr);

/**                                                                             
 *    @brief    Resumes media playback for the pipeline                              
 *    @param pipePtr    Pointer to Pipeline structure                                             
 *    @return    TRUE if success, FALSE otherwise                               
*/
gboolean DualDecode_resumeMedia (Pipeline *pipePtr);

/**                                                                             
 *    @brief    Sets the pipeline to READY state                              
 *    @param pipePtr    Pointer to Pipeline structure                                         
 *    @return    TRUE if success, FALSE otherwise                               
*/
gboolean DualDecode_stopMedia (Pipeline *pipePtr);

/**                                                                             
 *    @brief    Create a Pipeline structure                              
 *    @param void                                     
 *    @return Pipeline *pipe if success
              NULL if failure                                  
*/
Pipeline * DualDecode_createPipeline ();

/**                                                                             
 *    @brief    Seek media position in the pipeline                             
 *    @param pipePtr    Pointer to Pipeline structure    
 *    @param position   Position to be seeked                                     
 *    @return    TRUE if success, FALSE otherwise                               
*/
gboolean DualDecode_seekMedia(Pipeline *pipePtr, gint64 position);


/**                                                                             
 *    @brief    		destroy a Pipeline structure                              
 *    @param pipePtr	Pipeline structure                                     
*/
void DualDecode_destroyPipeline (Pipeline *pipePtr);

/**                                                                             
 *    @brief    		Get state of the pipeline
 *    @param pipePtr	Pipeline structure                                     
 *	  @return 			pipeline GstState
*/
GstState DualDecode_getMediaState (Pipeline *pipePtr);

/**                                                                             
 *    @brief    Get media position in the pipeline                             
 *    @param pipePtr    Pointer to Pipeline structure    
 *    @param seekScaleValue   Value of the seek scale bar                                     
 *    @param timeLabelText    Text to be displayed on the label. 
 *    @return    TRUE if success, FALSE otherwise                               
*/
gboolean DualDecode_getMediaPosition (Pipeline *pipePtr, 
                                      gdouble *seekScaleValue,
                                      gchar **timeLabelText);
/**                                                                             
 *    @brief    Switch decode mode from single to dual                             
 *    @param pipes    Pointer to an array consisting of both the pipelines   
 *    @param otherWindowSink  Video sink of the other window                                     
 *    @param thisWindow       Index of the window where switch button is clicked
 *    @param otherWindow      Index of the other window  
 *    @param filename         File to be played
 *    @return    TRUE if success, FALSE otherwise                               
*/
gboolean DualDecode_singleToDual (Pipeline **pipes, 
                                  GstElement * otherWindowSink,
                                  gint thisWindow, gint otherWindow,            
                                  gchar *filename);
/**                                                                             
 *    @brief    Switch decode mode from dual to single                             
 *    @param pipes    Pointer to an array consisting of both the pipelines   
 *    @param otherWindowSink  Video sink of the other window                                     
 *    @param thisWindow       Index of the window where switch button is clicked
 *    @param otherWindow      Index of the other window  
 *    @return    TRUE if success, FALSE otherwise                               
*/
gboolean DualDecode_dualToSingle (Pipeline **pipes,
                                  GstElement * otherWindowSink,
                                  gint thisWindow, gint otherWindow);
/**                                                                             
 *    @brief    Set video sinks of the pipeline                             
 *    @return    TRUE if success, FALSE otherwise                               
 *    @param pipePtr   Pointer to Pipeline structure    
 *    @param sink0     First sink of the pipeline                                
 *    @param sink1     Second sink of the pipeline
*/
void DualDecode_setPipelineSink (Pipeline *pipePtr, GstElement *sink0, 
                                 GstElement *sink1);



/******************************************************************************
 
                               Macros

 *****************************************************************************/
#define NANOSEC 1e9	
#define SS 0
#define MM 1
#define HH 2
#define TIMEOUT 4000000000

#endif /*__GST_CONTROLLER_H__*/  
